import React from "react";
import SlomuxContext from "./context";

export default ({ store, children }) => (
  <SlomuxContext.Provider value={store}>{children}</SlomuxContext.Provider>
);
