import React from "react";
import ReactDOM from "react-dom";

import Provider from "./slomux/provider";

import store from "./store";

import Timer from "./containers/timer";

ReactDOM.render(
  <Provider store={store}>
    <Timer />
  </Provider>,
  document.getElementById("root")
);
