import { CHANGE_INTERVAL } from "./constants";

export const changeInterval = (value) => {
  return { type: CHANGE_INTERVAL, payload: value };
};

export default {
  changeInterval,
};
