import React from "react";

import Interval from "../containers/interval";

class Timer extends React.Component {
  interval = null;

  state = {
    time: 0,
  };

  start = () => {
    this.interval = setInterval(() => {
      this.setState({
        time: this.state.time + this.props.interval,
      });
    }, this.props.interval * 1000);
  };

  stop = () => {
    clearInterval(this.interval);
    this.setState({ time: 0 });
  };

  render() {
    return (
      <div>
        <Interval />
        <span>Секундомер: {this.state.time} сек.</span>
        <span>
          <button onClick={this.start}>Старт</button>
          <button onClick={this.stop}>Стоп</button>
        </span>
      </div>
    );
  }
}

export default Timer;
