import React from "react";

class Interval extends React.Component {
  render() {
    return (
      <div>
        <span>Интервал обновления секундомера: {this.props.interval} сек.</span>
        <span>
          <button onClick={() => this.props.changeInterval(-1)}>-</button>
          <button onClick={() => this.props.changeInterval(1)}>+</button>
        </span>
      </div>
    );
  }
}

export default Interval;
