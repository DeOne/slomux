import connect from "../slomux/connect";
import { changeInterval } from "../actions";

import Interval from "../components/interval";

const mapStateToProps = (state) => ({
  interval: state,
});
const mapDispatchToProps = (dispatch) => ({
  changeInterval: (value) => dispatch(changeInterval(value)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Interval);
