import connect from "../slomux/connect";

import Timer from "../components/timer";

const mapStateToProps = (state) => ({
  interval: state,
});

export default connect(
  mapStateToProps,
  null
)(Timer);
